/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_next_line.c                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lorenuar <lorenuar@student.s19.be>         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/02/11 14:54:41 by lorenuar          #+#    #+#             */
/*   Updated: 2020/03/06 21:43:59 by lorenuar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "get_next_line.h"

/*
** ***** **
** DEBUG **
** ***** **
*/
#if DEBUG_F == 1
# include <stdio.h>
# define D printf(\
"\033[1;30;44m\n[%d] : '%s' | fd %d | b \"%s\" | l \"%s\"| pp %p | *pp %p | pt %p | p \"%s\" | t \"%s\" | ret %ld \n\033[0m",\
__LINE__, __FUNCTION__, fd, bufs[fd], *line, pline, *pline, tline, *pline, tline, ret);
#else
# define D ;
#endif

int				get_next_line(int fd, char **line)
{
	static char	bufs[FOPEN_MAX + 1][BUFFER_SIZE + 1];
	ssize_t		ret;
	char		*tline;

	tline = NULL;
	ret = 0;
	D;
	if (!hascharto(bufs[fd], '\n'))
	{
		D;
		while (!hascharto(bufs[fd], '\n') &&
				(ret = read(fd, &bufs[fd], BUFFER_SIZE)) &&
				!(bufs[fd][ret] = '\0'))
		{
			D;
			if (!hascharto(tline, '\n') &&
					(*tline = strjointo(tline, bufs[fd], '\0')) == -1)
			{
				D;
				return (-1);
			}
		}
	}
	else
	{
		D;
		ret = hascharto(bufs[fd], '\n');
		D;
		bufs[fd] = strjointo(&bufs[fd][ret], NULL, '\n');
	}
	if (tline)
	{
		free(tline);
		tline = NULL;
	}
	if (hascharto(bufs[fd], '\n'))
	{
		BR(LINE_RET)
		D;
		return ((*line = strjointo(bufs[fd], NULL, '\n')));
	}
	D;
	if (!ret)
	{
		D;
		return (0);
	}
	D;
	return (-1);
}
