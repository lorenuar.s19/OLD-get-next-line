/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_next_line.h                                    :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lorenuar <lorenuar@student.s19.be>         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/02/12 09:46:32 by lorenuar          #+#    #+#             */
/*   Updated: 2020/03/06 21:43:14 by lorenuar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#ifndef GET_NEXT_LINE_H
# define GET_NEXT_LINE_H

/*
** ******** **
** INCLUDES **
** ******** **
*/

# include <unistd.h>
# include <fcntl.h>
# include <stdlib.h>
# include <stddef.h>
# include <stdio.h>

/*
** ****** **
** MACROS **
** ****** **
*/
# ifndef BUFFER_SIZE
#  define BUFFER_SIZE 1024
# endif

# define DEBUG_U 1
# define DEBUG_F 1
# define DEBUG_M 1
# define BR(X) puts("\033[1;41m* "#X" *\n\033[0m"); getchar();

/*
** ********* **
** FUNCTIONS **
** ********* **
*/
int			get_next_line(int fd, char **line);

size_t		hascharto(char *s, char c);

char		*strjointo(char *s1, char *s2, char c);

#endif
