/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   get_next_line_utils.c                              :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lorenuar <lorenuar@student.s19.be>         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/02/12 09:45:33 by lorenuar          #+#    #+#             */
/*   Updated: 2020/03/06 21:43:50 by lorenuar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "get_next_line.h"

/*
** ***** **
** DEBUG **
** ***** **
*/
#if DEBUG_U == 1
# include <stdio.h>
# define D printf(\
"\n\033[1;42;30m[%d] : '%s' | a \"%s\" | i %lu | lt1 %ld | s1 \"%s\" | lt2 %ld | s2 \"%s\"\n\033[0m",\
__LINE__, __FUNCTION__, a, i, lt1, s1, lt2, s2);
# define DD printf("\033[1;30;43m\n[%d] : '%s' | \"%s\" | c %d | lt %lu\n\033[0m",\
					__LINE__, __FUNCTION__, s, c, lento);
#else
# define D ;
# define DD ;
#endif

/*
** returns lento if s contains c
*/

size_t		hascharto(char *s, char c)
{
	size_t	lento;

	lento = 0;
	DD;
	while (s && s[lento])
	{
		if(c == s[lento++])
		{
			DD;
			return (lento);
		}
	}
	DD;
	if (s && !s[lento])
	{
		DD;
		return (lento);
	}
	DD;
	return (0);
}

/*
** allocate string until c, using strlento
*/

char	*strjointo(char *s1, char *s2, char c)
{
	char	*a;
	ssize_t	lt1;
	ssize_t	lt2;
	ssize_t	i;

	lt1 = hascharto(s1, c);
	lt2 = hascharto(s2, c);
	i = 0;
	a = NULL;
	if (!(a = malloc((lt1 + lt2) + 1)))
	{
		D;
		BR(JOIN_ERROR);
		return (-1);
	}
	while (lt1 && s1 && *s1 && *s1 != c && i < lt1)
		a[i++] = *s1++;
	while (lt2 && s2 && *s2 && *s2 != c && i < (lt1 + lt2))
		a[i++] = *s2++;
	a[i++] = '\0';
	**ret = a;
	D;
	BR(JOIN_END);
	if (i == 1)
		return (0);
	return (1);
}
