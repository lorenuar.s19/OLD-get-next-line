/* ************************************************************************** */
/*                                                                            */
/*                                                        :::      ::::::::   */
/*   main.c                                             :+:      :+:    :+:   */
/*                                                    +:+ +:+         +:+     */
/*   By: lorenuar <lorenuar@student.s19.be>         +#+  +:+       +#+        */
/*                                                +#+#+#+#+#+   +#+           */
/*   Created: 2020/02/21 18:28:46 by lorenuar          #+#    #+#             */
/*   Updated: 2020/02/22 18:42:53 by lorenuar         ###   ########.fr       */
/*                                                                            */
/* ************************************************************************** */

#include "get_next_line.h"

void	ps(char *s)
{
	printf("\n>> ");
	while (s && *s)
	{
		printf("%c:%d", *s, *s);
		if (*s++)
			printf(" | ");
	}
	printf(" <<\n\n");
}

int		main(int argc, char **argv)
{
	int	fd;
	int	ret;
	char	*line = NULL;
	fd = 0;
	
	printf("(%d)\n", FOPEN_MAX);
	if (argc == 2)
		fd = open(argv[1], O_RDONLY);
	while ((ret = get_next_line(fd, &line)))
	{
		printf("|\n> r %d | \"%s\"\n|\n", ret, line);
		ps(line);
		BR(MAIN_LOOP)
		free(line);
		line = NULL;
	}
	BR(MAIN_LAST)
	printf("|\n> r %d | \"%s\"\n|\n", ret, line);
	ps(line);
	free(line);
	line = NULL;
	return (0);

}
